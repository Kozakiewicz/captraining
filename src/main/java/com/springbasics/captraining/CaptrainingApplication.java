package com.springbasics.captraining;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class CaptrainingApplication {

	public static void main(String[] args) {
		SpringApplication.run(CaptrainingApplication.class, args);
	}

}
