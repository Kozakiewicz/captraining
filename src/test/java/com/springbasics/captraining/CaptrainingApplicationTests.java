package com.springbasics.captraining;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;

@AutoConfigureMockMvc
@SpringBootTest(classes = OrderingApp.class)
class CaptrainingApplicationTests {

    @Autowired
    private MockMvc mockMvc;

    @Test
    void doPost() throws Exception {
        mockMvc.perform(MockMvcRequestBuilders.post("/do")
                .contentType(MediaType.APPLICATION_JSON)
                .content("{\"orderId\":\"123\"}"))
                .andExpect(MockMvcResultMatchers.status().is2xxSuccessful())
                .andExpect(MockMvcResultMatchers.content().string("placed order: 123"));
    }


}
