package com.springbasics.captraining;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class OrderController {


    private OrderingApp.OrderService orderService;

    public OrderController(OrderingApp.OrderService orderService) {
        this.orderService = orderService;
    }

    @PostMapping("/do")
    public String placeOrderEndpoint(@RequestBody OrderingApp.OrderRequest orderRequest) {
        return orderService.placeOrder(orderRequest);
    }

    @GetMapping("/test")
    public String testMethod() {
        return "SUCCESS";
    }
}
