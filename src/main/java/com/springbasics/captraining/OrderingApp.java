package com.springbasics.captraining;

import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.ConfigurableApplicationContext;
import org.springframework.context.annotation.*;
import org.springframework.stereotype.Component;

@SpringBootApplication
public class OrderingApp {


    public static void main(String[] args) {
        ConfigurableApplicationContext context = SpringApplication.run(OrderingApp.class, args);
//        AnnotationConfigApplicationContext context =
//                new AnnotationConfigApplicationContext("com.springbasics.captraining");
        OrderService orderService = context.getBean(OrderService.class);
        orderService.payOrder(); // BM

        // OrderServiceNew, PayU
        OrderServiceNew orderServiceNew = context.getBean(OrderServiceNew.class);
        orderServiceNew.payOrder(); // PayU

    }

    @Component
    static class OrderServiceNew {

        private PaymentProvider paymentProvider;

        public OrderServiceNew(@Qualifier("newPayment") PaymentProvider paymentProvider) {
            this.paymentProvider = paymentProvider;
        }

        void payOrder() {
            System.out.println("payOrder");
            paymentProvider.makePayment();
        }
    }


    @Configuration
    static class AppConfig {

        @Bean("newPayment")
        public PaymentProvider payUPaymentProvider() {
            return new PayUPaymentProvider();
        }

    }

    @Component
    static class OrderService {

        private PaymentProvider paymentProvider;

        public OrderService(PaymentProvider paymentProvider) {
            this.paymentProvider = paymentProvider;
        }

        void payOrder() {
            System.out.println("payOrder");
            paymentProvider.makePayment();
        }

        String placeOrder(OrderRequest request) {
            return "placed order: " + request.orderId;
        }

    }


    interface PaymentProvider {
        void makePayment();
    }

    static class PayUPaymentProvider implements PaymentProvider {

        public void makePayment() {
            System.out.println("PAYU");
        }
    }

    @Primary
    @Component
    static class BlueMediaPaymentProvider implements PaymentProvider {
        public void makePayment() {
            System.out.println("BM");
        }
    }


    static class OrderRequest {
        int orderId;

        public void setOrderId(int orderId) {
            this.orderId = orderId;
        }

        // Nie dziala mapowanie przez konstruktor, dlaczego? Jakas flaga do przestawienia?
//        public OrderRequest(int orderId) {
//            this.orderId = orderId;
//        }

        @Override
        public String toString() {
            return "OrderRequest{" +
                    "orderId=" + orderId +
                    '}';
        }
    }
}